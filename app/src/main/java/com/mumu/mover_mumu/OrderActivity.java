package com.mumu.mover_mumu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.mumu.mover_mumu.adapter.OrderListAdapter;
import com.mumu.mover_mumu.model.OrderListModel;
import com.mumu.mover_mumu.utils.RestClient;

import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class OrderActivity extends AppCompatActivity {

    private Subscription mRvOrdersSubscription;
    private RecyclerView mRvOrders;
    private ProgressBar mProgressBar;
    private OrderListAdapter mOrderListAdapter;
   private RestClient mRestClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRestClient = new RestClient(this);
        configureLayout();
        createObservable();
    }

    //TODO: Use RxAndroid for Async / non blocking I/O and Volley for Networking
    private void createObservable() {
        Observable<List<OrderListModel>> rvShowOrderObservable = Observable.fromCallable(new Callable<List<OrderListModel>>() {
            @Override
            public List<OrderListModel> call() throws Exception {
                return mRestClient.getOrders();
            }
        });

        mRvOrdersSubscription = rvShowOrderObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<OrderListModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<OrderListModel> orderListModels) {
                        displayRvOrders(orderListModels);
                    }
                });



    }

    private void displayRvOrders(List<OrderListModel> orderListModels) {
        mOrderListAdapter.setmOrderLists(orderListModels);
        mProgressBar.setVisibility(View.GONE);
        mRvOrders.setVisibility(View.VISIBLE);
    }



    private void configureLayout() {
        setContentView(R.layout.activity_order);
        mProgressBar = (ProgressBar) findViewById(R.id.loader);
        mRvOrders = (RecyclerView) findViewById(R.id.rv_order_list);
        mRvOrders.setLayoutManager(new LinearLayoutManager(this));
        mOrderListAdapter = new OrderListAdapter(this);
        mRvOrders.setAdapter(mOrderListAdapter);

    }
}
