package com.mumu.mover_mumu.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import android.content.Context;

import com.mumu.mover_mumu.model.OrderListModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by oneal on 30/10/16.
 */

public class RestClient {
    private Context mContext;

    public RestClient(Context mContext) {
        this.mContext = mContext;
    }

    public List<OrderListModel> getOrders() {
        try {
            //Simulate the delay of network
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return getOrderList();
    }

    private List<OrderListModel> getOrderList() {
        List<OrderListModel> orderList = new ArrayList<>();
        try {
            List<OrderListModel> collectOrderList = new ArrayList<>();
           /* for (int i = 0; i < Config.dataOrderListDummy().getJSONArray("order").length(); i++) {
                JSONObject orderObject = Config.dataOrderListDummy().getJSONArray("order").getJSONObject(i);
                //Populate the Order
                OrderListModel orderListModel = new OrderListModel(
                        orderObject.getString("order_id"),
                        orderObject.getString("region"),
                        orderObject.getInt("store_id"),
                        orderObject.getString("store_name"),
                        orderObject.getInt("time_slot_id"),
                        orderObject.getString("time_slot"),
                        orderObject.getString("delivery_address"),
                        orderObject.getString("tlp"),
                        orderObject.getInt("distance"),
                        orderObject.getBoolean("isConfirmed"),
                        orderObject.getString("confirmedBy")
                );
                orderList.add(orderListModel);*/

            //Parsing menggunakan GSON Simple & something fresh than native
            Gson gson = new Gson();
            String json = new String(Config.dataOrderListDummy().getJSONArray("order").toString());
            orderList = Arrays.asList(gson.fromJson(json, OrderListModel[].class));

        } catch (JSONException e) {
        }


        return orderList;
    }
}
