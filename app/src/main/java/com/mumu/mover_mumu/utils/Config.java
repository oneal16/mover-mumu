package com.mumu.mover_mumu.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by oneal on 30/10/16.
 */

public class Config {

    public static JSONObject dataOrderListDummy() throws JSONException {
        return new JSONObject("{\n" +
                "  \"message\": \"Success\",\n" +
                "  \"order\": [\n" +
                "    {\n" +
                "      \"order_id\": \"xyzabc12\",\n" +
                "      \"region\": \"Jakarta Selatan\",\n" +
                "      \"store_id\": 8,\n" +
                "      \"store_name\": \"Carrefour\",\n" +
                "      \"time_slot_id\": 482,\n" +
                "      \"time_slot\": \"20:00-21:00\",\n" +
                "      \"delivery_address\": \"Jl HR. Rasuna Said Kav 10 Jakarta Selatan \",\n" +
                "      \"tlp\": \"08118696969\",\n" +
                "      \"distance\": 4,\n" +
                "      \"isConfirmed\": true,\n" +
                "      \"confirmedBy\": \"Name_of_shopper\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"order_id\": \"xyzabc12\",\n" +
                "      \"region\": \"Jakarta Selatan\",\n" +
                "      \"store_id\": 8,\n" +
                "      \"store_name\": \"Carrefour\",\n" +
                "      \"time_slot_id\": 482,\n" +
                "      \"time_slot\": \"20:00-21:00\",\n" +
                "      \"delivery_address\": \"Jl HR. Rasuna Said Kav 10 Jakarta Selatan \",\n" +
                "      \"tlp\": \"08118696969\",\n" +
                "      \"distance\": 5,\n" +
                "      \"isConfirmed\": false,\n" +
                "      \"confirmedBy\": null\n" +
                "    },\n" +
                "    {\n" +
                "      \"order_id\": \"xyzabc12\",\n" +
                "      \"region\": \"Jakarta Selatan\",\n" +
                "      \"store_id\": 8,\n" +
                "      \"store_name\": \"Carrefour\",\n" +
                "      \"time_slot_id\": 482,\n" +
                "      \"time_slot\": \"20:00-21:00\",\n" +
                "      \"delivery_address\": \"Jl HR. Rasuna Said Kav 10 Jakarta Selatan \",\n" +
                "      \"tlp\": \"08118696969\",\n" +
                "      \"distance\": 6,\n" +
                "      \"isConfirmed\": true,\n" +
                "      \"confirmedBy\": \"Name_of_shopper\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"order_id\": \"xyzabc12\",\n" +
                "      \"region\": \"Jakarta Selatan\",\n" +
                "      \"store_id\": 8,\n" +
                "      \"store_name\": \"Carrefour\",\n" +
                "      \"time_slot_id\": 482,\n" +
                "      \"time_slot\": \"20:00-21:00\",\n" +
                "      \"delivery_address\": \"Jl HR. Rasuna Said Kav 10 Jakarta Selatan \",\n" +
                "      \"tlp\": \"08118696969\",\n" +
                "      \"distance\": 7,\n" +
                "      \"isConfirmed\": true,\n" +
                "      \"confirmedBy\": \"Name_of_shopper\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"order_id\": \"xyzabc12\",\n" +
                "      \"region\": \"Jakarta Selatan\",\n" +
                "      \"store_id\": 8,\n" +
                "      \"store_name\": \"Carrefour\",\n" +
                "      \"time_slot_id\": 482,\n" +
                "      \"time_slot\": \"20:00-21:00\",\n" +
                "      \"delivery_address\": \"Jl HR. Rasuna Said Kav 10 Jakarta Selatan \",\n" +
                "      \"tlp\": \"08118696969\",\n" +
                "      \"distance\": 8,\n" +
                "      \"isConfirmed\": false,\n" +
                "      \"confirmedBy\": null\n" +
                "    },\n" +
                "    {\n" +
                "      \"order_id\": \"xyzabc12\",\n" +
                "      \"region\": \"Jakarta Selatan\",\n" +
                "      \"store_id\": 8,\n" +
                "      \"store_name\": \"Carrefour\",\n" +
                "      \"time_slot_id\": 482,\n" +
                "      \"time_slot\": \"20:00-21:00\",\n" +
                "      \"delivery_address\": \"Jl HR. Rasuna Said Kav 10 Jakarta Selatan \",\n" +
                "      \"tlp\": \"08118696969\",\n" +
                "      \"distance\": 4,\n" +
                "      \"isConfirmed\": false,\n" +
                "      \"confirmedBy\": null\n" +
                "    },\n" +
                "    {\n" +
                "      \"order_id\": \"xyzabc12\",\n" +
                "      \"region\": \"Jakarta Selatan\",\n" +
                "      \"store_id\": 8,\n" +
                "      \"store_name\": \"Carrefour\",\n" +
                "      \"time_slot_id\": 482,\n" +
                "      \"time_slot\": \"20:00-21:00\",\n" +
                "      \"delivery_address\": \"Jl HR. Rasuna Said Kav 10 Jakarta Selatan \",\n" +
                "      \"tlp\": \"08118696969\",\n" +
                "      \"distance\": 9,\n" +
                "      \"isConfirmed\": false,\n" +
                "      \"confirmedBy\": null\n" +
                "    }\n" +
                "  ]\n" +
                "}");
    }
}
