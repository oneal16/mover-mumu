package com.mumu.mover_mumu.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mumu.mover_mumu.MapsActivity;
import com.mumu.mover_mumu.R;
import com.mumu.mover_mumu.model.OrderListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oneal on 30/10/16.
 */
public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListItemViewHolder>{
    private final Context mContext;
    private final List<OrderListModel> mOrderLists = new ArrayList<>();

    public OrderListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setmOrderLists(List<OrderListModel> newOrderList){
        mOrderLists.clear();
        mOrderLists.addAll(newOrderList);
        notifyDataSetChanged();
    }


    @Override
    public OrderListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_order_layout, parent, false);
        return new OrderListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderListItemViewHolder holder, int position) {
        holder.tvOrderId.setText(mOrderLists.get(position).getOrder_id());
        holder.tvCity.setText(mOrderLists.get(position).getRegion());
        holder.tvNameStore.setText(mOrderLists.get(position).getStore_name());
        holder.tvDeliverySlot.setText(mOrderLists.get(position).getTime_slot());
        holder.tvDeliveryAddress.setText(mOrderLists.get(position).getDelivery_address());
        holder.tvDistance.setText(String.valueOf(mOrderLists.get(position).getDistance())+" Km");
        if (mOrderLists.get(position).isConfirmed()){
            holder.tvConfirmedBy.setText(mOrderLists.get(position).getConfirmedBy());
            holder.btnConfirm.setEnabled(false);
            holder.btnConfirm.setClickable(false);
            holder.btnConfirm.setText(R.string.confirmed);
            holder.btnConfirm.setTextColor(mContext.getResources().getColor(R.color.dark_gray));
            holder.btnConfirm.setBackground(mContext.getResources().getDrawable(R.drawable.button_green_invert_grey));
            holder.ibMap.setEnabled(false);
            holder.ibMap.setClickable(false);
        } else {
            holder.rlConfirmedShopper.setVisibility(View.GONE);
            holder.btnConfirm.setEnabled(true);
            holder.btnConfirm.setClickable(true);
            holder.btnConfirm.setText(R.string.confirmation);
            holder.btnConfirm.setTextColor(mContext.getResources().getColor(R.color.mumu_branding_green));
            holder.btnConfirm.setBackground(mContext.getResources().getDrawable(R.drawable.button_green_invert));
            holder.ibMap.setEnabled(true);
            holder.ibMap.setEnabled(true);
        }

        holder.ibMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MapsActivity.class);
                mContext.startActivity(intent);
            }
        });
        holder.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "Hoola, Confirmation Order!", Toast.LENGTH_SHORT).show();
            }
        });



    }

    @Override
    public int getItemCount() {
        return mOrderLists.size();
    }

    public class OrderListItemViewHolder extends RecyclerView.ViewHolder{
        TextView tvOrderId;
        TextView tvCity;
        TextView tvNameStore;
        TextView tvDeliverySlot;
        TextView tvDeliveryAddress;
        TextView tvDistance;
        TextView tvConfirmedBy;
        TextView tvConfirmedByValue;
        ImageButton ibMap;
        RelativeLayout rlConfirmedShopper;

        Button btnConfirm;

        public OrderListItemViewHolder(View itemView) {
            super(itemView);
            tvOrderId = (TextView) itemView.findViewById(R.id.tv_order_id_value);
            tvCity = (TextView) itemView.findViewById(R.id.tv_city_value);
            tvNameStore = (TextView) itemView.findViewById(R.id.tv_store_value);
            tvDeliverySlot =(TextView) itemView.findViewById(R.id.tv_delivery_slot_value);
            tvDeliveryAddress = (TextView) itemView.findViewById(R.id.tv_delivery_address_value);
            tvDistance = (TextView) itemView.findViewById(R.id.tv_distance_value);
            btnConfirm = (Button) itemView.findViewById(R.id.btn_confirm_order);
            rlConfirmedShopper = (RelativeLayout) itemView.findViewById(R.id.rl_confirmed_shopper);
            tvConfirmedBy = (TextView) itemView.findViewById(R.id.tv_confirmedBy_value);
            ibMap = (ImageButton) itemView.findViewById(R.id.imageButton);
        }
    }
}
