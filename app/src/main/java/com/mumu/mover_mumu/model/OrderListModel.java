package com.mumu.mover_mumu.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by oneal on 30/10/16.
 */
public class OrderListModel {
    @SerializedName("order_id")
    @Expose
    String order_id;
    @SerializedName("region")
    @Expose
    String region;
    @SerializedName("store_id")
    @Expose
    int store_id;
    @SerializedName("store_name")
    @Expose
    String store_name;
    @SerializedName("time_slot_id")
    @Expose
    int time_slot_id;
    @SerializedName("time_slot")
    @Expose
    String time_slot;
    @SerializedName("delivery_address")
    @Expose
    String delivery_address;
    @SerializedName("telp")
    @Expose
    String telp;
    @SerializedName("distance")
    @Expose
    int distance;
    @SerializedName("isConfirmed")
    @Expose
    boolean isConfirmed;
    @SerializedName("confirmedBy")
    @Expose
    String confirmedBy;

    List<OrderListModel> orderListModels;

    public List<OrderListModel> getOrderListModels() {
        return orderListModels;
    }

    public void setOrderListModels(List<OrderListModel> orderListModels) {
        this.orderListModels = orderListModels;
    }

    public OrderListModel(String order_id, String region, int store_id, String store_name, int time_slot_id, String time_slot, String delivery_address, String telp, int distance, boolean isConfirmed, String confirmedBy) {
        this.order_id = order_id;
        this.region = region;
        this.store_id = store_id;
        this.store_name = store_name;
        this.time_slot_id = time_slot_id;
        this.time_slot = time_slot;
        this.delivery_address = delivery_address;
        this.telp = telp;
        this.distance = distance;
        this.isConfirmed = isConfirmed;
        this.confirmedBy = confirmedBy;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public int getTime_slot_id() {
        return time_slot_id;
    }

    public void setTime_slot_id(int time_slot_id) {
        this.time_slot_id = time_slot_id;
    }

    public String getTime_slot() {
        return time_slot;
    }

    public void setTime_slot(String time_slot) {
        this.time_slot = time_slot;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public String getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(String confirmedBy) {
        this.confirmedBy = confirmedBy;
    }
}
